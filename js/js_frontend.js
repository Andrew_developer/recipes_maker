jQuery(document).ready(function ($) {
    var $formNotice = $('.form-notice');
    var $imgForm    = $('.image-form');
    var $imgNotice  = $imgForm.find('.image-notice');
    var $imgPreview = $imgForm.find('.image-preview');
    var $imgFile    = $imgForm.find('.image-file');
    var $imgId      = $imgForm.find('[name="image_id"]');

    $('body').on('change', '.image-file', function(e) {
        e.preventDefault();

        var formData = new FormData();

        var $this = $(this);

        formData.append('action', 'upload-attachment');
        formData.append('async-upload', $(this)[0].files[0]);
        formData.append('name', $(this)[0].files[0].name);
        formData.append('_wpnonce', su_config.nonce);

        $.ajax({
            url: su_config.upload_url,
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            type: 'POST',
            success: function(resp) {
                var src = '<img width="50" src="' + resp.data.url  + '">';

                $this.parent().find('.image-preview').html(src);

                $this.parent().find('.image-input').val(resp.data.url);
            }
        });
    });
});

