jQuery(document).ready(function ($) {

    function add_preset(id, name) {
        var array = $('#select-cat-recipes').val();
        var preset = '[recipes_maker_shortcode cat="all"]';

        if (array !== null) {
            var categories = array.join(',');

            preset = '[recipes_maker_shortcode cat="' + categories + '"]';
        }
        else {
            preset = '[recipes_maker_shortcode cat="all"]';
        }

        wp.media.editor.insert(preset);
    }


// metabox functions

    var $ingredientsInfo = $('.metabox-recipes-info');
    var $list;
    var $item;

    function addItems(elem, meta) {
        $list = $(elem).closest('.metabox-recipes-info').find('.recipes-item-list');
        $item = $list.find('.item-metabox').last().clone();

        var iteration = $item.find('input').first().attr('data-iter');

        iteration = parseInt(iteration) + 1;

        if (meta == true) {
            $item = $list.find('.item-metabox').not('.headline-item').last().clone();
        }
        else {
            if($item.find('.title-headline').length == 0){
                $item.prepend('<span class="title-headline">headline</span>');
            }

            $item.addClass('headline-item');
            $item.find('img').remove();
            $item.find('button').remove();
        }

        var html = $item.html();
        html = html.replace(/\d+/g, iteration);

        $item.html(html);
    }

    $('.add-custom-metabox').click(function (e) {
        e.stopPropagation();

        addItems(this, true);

        $item.find('input').val('');
        $item.find('img').attr({'src': ''});

        $list.append($item);

        return null;
    });

    $('.add-headline-btn').click(function (e) {
        e.stopPropagation();

        addItems(this, false);

        $item.find('input').val('');
        $item.find('input').last().val('headline');

        $list.append($item);
    });

    $ingredientsInfo.on('click', '.remove-metabox-rec', function (e) {
        e.stopPropagation();

        var listItems = $(this).closest('.metabox-recipes-info').find('.item-metabox');

        if (listItems.length > 1) {
            $(this).closest('.item-metabox').remove();
        }
        else {
            $(this).closest('.item-metabox').find('input').val('');
        }
    });

    var meta_image_frame;


});
