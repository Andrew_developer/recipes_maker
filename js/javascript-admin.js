jQuery(document).ready(function ($) {
    $('.popup-button-recipces').magnificPopup({
        type: 'inline',
        midClick: true
    });

    $('.insert-shortdode-btn').click(function () {
        add_preset();

        var magnificPopup = $.magnificPopup.instance;
        magnificPopup.close();
    });

    var meta_image_frame;

    $('body').on('click', '.add-image-rec', function (e) {
        var parent = $(this).parent();

        meta_image_frame = wp.media.frames.meta_image_frame = wp.media({});

        meta_image_frame.on('select', function () {
            var media_attachment = meta_image_frame.state().get('selection').first().toJSON();

            parent.find('.image-preview').attr({'src': media_attachment.url});
            parent.find('.image-preview').attr({'width': 50});
            parent.find('.image-input').val(media_attachment.url);
        });

        meta_image_frame.open();
    });

    $("#sortable-i, #sortable-p").sortable();
    $("#sortable").disableSelection();
});