<table class="form-table metabox-recipes-info">

    <tr>
        <th>
            Process Add <span class="dashicons dashicons-plus-alt add-custom-metabox"></span>

            <button type="button" class="add-headline-btn ui-button">Add Headline</button>
        </th>
    </tr>

    <tr>
        <td class="recipes-item-list" id="sortable-i">

            <span class="item-metabox ui-state-default">
                <input data-iter="%s" type="text" name="process[0][des]">

                <input class="image-input" type="hidden" name="process[0][image]" value="">

                <span class="dashicons dashicons-trash remove-metabox-rec"></span>

                <img class="image-preview" width="50" src="">

                <p class="image-notice"></p>
                <input type="file" name="async-upload" class="image-file" accept="image/*" required>
                <input type="hidden" name="image_id">
                <input type="hidden" name="action" value="image_submission">
                <div class="image-preview"></div>
            </span>

        </td>

    </tr>
</table>
