<div class="col-lg-5 col-md-5">
    <?php the_post_thumbnail();


    $RCP_MK_rating->rating(); ?>
</div>

<div class="col-lg-5 col-md-5">
    <header class="entry-header">
        <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
    </header>

    <div class="additional-recipes-metabox">
        <h5>Recipe information</h5>

        <?php $additional = get_post_meta($post->ID, 'additional', true); ?>

        <ul>
            <li><strong>Servings:</strong> <?php echo $additional[0]; ?></li>

            <li><strong>Calories:</strong> <?php echo $additional[1]; ?></li>

            <li><strong>Prep Time:</strong> <?php echo $additional[2]; ?></li>

            <li><strong>Cook Time:</strong> <?php echo $additional[3]; ?></li>

            <li><strong>Total Time:</strong> <?php echo $additional[4]; ?></li>
        </ul>
    </div>

    <div class="nutrition-rec">
        <h5>Nutrition Fact</h5>
        <?php
        $typeNat = get_post_meta($post->ID, 'nutrition', true);
        if ($typeNat) {
            $nutrition = array_chunk($typeNat, 2); ?>

            <ul>
                <?php foreach ($nutrition as $key => $value) { ?>

                    <li><strong><?php echo $value[0] ?>:</strong> <?php echo $value[1] ?>
                    </li>

                <?php } ?>
            </ul>
        <?php } ?>

    </div>

    <div class="entry-content">
        <?php the_content(); ?>
    </div>
</div>

<div class="custom-fields-recipes">
    <div class="ingredients-rec col-lg-5">
        <h5>Ingredients</h5>

        <?php $ingredients = get_post_meta($post->ID, 'ingredients', true);

        foreach ($ingredients as $key => $value) { ?>
            <div class="ingredient-item-rec">
                <?php if ($value['image'] == 'headline') { ?>
                    <h4> <?php echo $value['des']; ?> </h4>

                <?php } else { ?>
                    <span><?php echo $value['des']; ?></span>
                <?php } ?>
            </div>
        <?php } ?>
    </div>

    <div class="process-recipes col-lg-5">
        <h5>Process</h5>

        <?php $process = get_post_meta($post->ID, 'process', true);

        foreach ($process as $key => $value) { ?>
            <div class="process-item-rec">
                <?php if ($value['image'] == 'headline') { ?>

                    <h4> <?php echo $value['des']; ?> </h4>

                <?php } else { ?>
                    <img width="150" src="<?php echo $value['image']; ?>">

                    <span><?php echo $value['des']; ?></span>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>