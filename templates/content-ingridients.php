<table class="form-table table-added-recipe metabox-recipes-info">
    <tr>
        <th>
            Ingredients add <span class="dashicons dashicons-plus-alt add-custom-metabox"></span>

            <button type="button" class="add-headline-btn ui-button">Add Headline</button>
        </th>
    </tr>

    <tr>
        <td class="recipes-item-list" id="sortable-i">

            <span class="item-metabox ui-state-default">
                <input data-iter="1" type="text" name="ingredients[1][des]">

                <input class="image-input" type="hidden" name="ingredients[1][image]">

                <span class="dashicons dashicons-trash remove-metabox-rec"></span>

                <p class="image-notice"></p>
                <input type="file" name="async-upload" class="image-file" accept="image/*" required>
                <input type="hidden" name="image_id">
                <input type="hidden" name="action" value="image_submission">
                <div class="image-preview"></div>
            </span>

        </td>
    </tr>
</table>
