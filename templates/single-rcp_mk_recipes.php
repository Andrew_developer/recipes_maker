<?php

get_header(); ?>


<div class="wrap">
    <section id="primary" class="content-area recipe-content">
        <main id="main" class="site-main">
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="container">
                    <div class="row">
                        <?php while (have_posts()) : the_post(); ?>

                            <?php include 'content-recipe.php'; ?>

                        <?php endwhile; ?>
                    </div>
                </div>
            </article>
        </main>
    </section>
</div>


<?php get_footer(); ?>
