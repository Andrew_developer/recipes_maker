<?php
/*
Plugin Name: Recipes Maker
Description: Recipes Maker
Version: 1.0
Author URI: https://google.com
Author: Andrew
*/

include dirname(__FILE__) . '/manage/assets.php';
include dirname(__FILE__) . '/shortcode.php';
include dirname(__FILE__) . '/helpers/metabox.php';
include dirname(__FILE__) . '/helpers/metabox_ingredients.php';
include dirname(__FILE__) . '/helpers/metabox_process.php';
include dirname(__FILE__) . '/helpers/nutrition-facts.php';
include dirname(__FILE__) . '/helpers/metabox_custom.php';
include dirname(__FILE__) . '/helpers/metabox-video.php';
include dirname(__FILE__) . '/helpers/rating.php';
include dirname(__FILE__) . '/manage/manage.php';

register_activation_hook(__FILE__, 'recipes_maker_activation_mod');
register_deactivation_hook(__FILE__, 'recipes_maker_deactivation_mod');
add_action('admin_menu', 'recipes_maker_admin_menu');
add_action('admin_init', 'recipes_maker_admin_settings');

function recipes_maker_activation_mod()
{

}

function recipes_maker_deactivation_mod()
{

}

add_action('init', 'register_post_type_recipes_maker');
function register_post_type_recipes_maker()
{
    register_post_type('rcp_mk_recipes', array(
        'label' => null,
        'labels' => array(
            'name' => 'Recipes',
            'singular_name' => 'Recipes',
            'add_new' => 'Add recipe',
            'add_new_item' => 'Add recipe',
            'edit_item' => 'Edit recipe',
            'new_item' => 'New recipe',
            'view_item' => 'Show recipe',
            'search_items' => 'Find recipes',
            'not_found' => 'not found',
            'not_found_in_trash' => 'not found',
            'parent_item_colon' => '',
            'menu_name' => 'Recipes',
        ),
        'description' => '',
        'public' => true,
        'publicly_queryable' => null,
        'exclude_from_search' => null,
        'show_ui' => true,
        'show_in_menu' => null,
        'show_in_admin_bar' => null,
        'show_in_nav_menus' => null,
        'show_in_rest' => null,
        'rest_base' => null,
        'menu_position' => 55,
        'menu_icon' => null,
        'hierarchical' => false,
        'supports' => array('title', 'editor', 'thumbnail', 'author'),
        'taxonomies' => array(),
        'has_archive' => false,
        'rewrite' => true,
        'query_var' => true,
    ));
}

add_action('init', 'create_taxonomy');
function create_taxonomy()
{
    register_taxonomy('rcp_mk_recipes_cat', array('rcp_mk_recipes'), array(
        'label' => '',
        'labels' => array(
            'name' => 'Recipes Category',
            'singular_name' => 'Recipes category',
            'search_items' => 'Search recipes category',
            'all_items' => 'All recipes category',
            'view_item ' => 'View recipes category',
            'parent_item' => 'Parent recipes category',
            'parent_item_colon' => 'Parent recipes category:',
            'edit_item' => 'Edit recipes category',
            'update_item' => 'Update recipes category',
            'add_new_item' => 'Add New recipes category',
            'new_item_name' => 'New Genre Recipes category',
            'menu_name' => 'Recipes category',
        ),
        'description' => '',
        'public' => true,
        'publicly_queryable' => null,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_in_menu' => null,
        'show_tagcloud' => true,
        'show_in_rest' => true,
        'rest_base' => null,
        'hierarchical' => true,
        'update_count_callback' => '',
        'rewrite' => true,
        'capabilities' => array(),
        'meta_box_cb' => null,
        'show_admin_column' => true,
        '_builtin' => false,
        'show_in_quick_edit' => null,
    ));

}

function recipes_maker_load_recipe_template($template) {
    global $post;

    if ($post->post_type == "rcp_mk_recipes" && $template !== locate_template(array("single-rcp_mk_recipes.php"))){
        return plugin_dir_path( __FILE__ ) . "templates/single-rcp_mk_recipes.php";
    }

    return $template;
}

add_filter('single_template', 'recipes_maker_load_recipe_template');

function recipes_maker_admin_menu()
{
    add_submenu_page(
        'edit.php?post_type=rcp_mk_recipes',
        'Fund Settings',
        'Settings',
        'manage_options',
        'settings',
        'recipes_maker_options_menu'
    );
}

function recipes_maker_admin_settings()
{
    register_setting('rcp_mk_settings', 'rcp_mk_options_template', 'sanitize_callback');

    add_settings_section('recipes_section_id', 'Settings plugin recipes', '', 'recipes-options');

    add_settings_field('rcp_mk_templates', 'template for recipes', 'fill_rcp_mk_templates', 'recipes-options', 'recipes_section_id' );
}

function fill_rcp_mk_templates(){
    $val = get_option('rcp_mk_options_template');

    $val = $val ? $val['radio'] : null; ?>

    <div class="list-templates-recipes">
        <div>
            <label>
                <input type="radio" name="rcp_mk_options_template[radio]" value="1" <?php checked( 1, $val ) ?> />
            </label>

            <img src="<?php echo plugins_url('', __FILE__) . '/image/11.jpg' ?>">
        </div>

        <div>
            <label>
                <input type="radio" name="rcp_mk_options_template[radio]" value="2" <?php checked( 2, $val ) ?> />
            </label>

            <img src="<?php echo plugins_url('', __FILE__) . '/image/22.jpg' ?>">
        </div>

        <div>
            <label>
                <input type="radio" name="rcp_mk_options_template[radio]" value="3" <?php checked( 3, $val ) ?> />
            </label>

            <img src="<?php echo plugins_url('', __FILE__) . '/image/11.jpg' ?>">
        </div>

        <div>
            <label>
                <input type="radio" name="rcp_mk_options_template[radio]" value="4" <?php checked( 4, $val ) ?> />
            </label>

            <img src="<?php echo plugins_url('', __FILE__) . '/image/22.jpg' ?>">
        </div>
    </div>

    <?php
}


function sanitize_callback( $options ){
    foreach( $options as $name => & $val ){
        if( $name == 'radio' )
            $val = strip_tags( $val );

        if( $name == 'checkbox' )
            $val = intval( $val );
    }

    return $options;
}

function recipes_maker_options_menu()
{ ?>
    <h1>Recipes settings:</h1>

    <form action="options.php" method="POST">
        <?php
            settings_fields( 'rcp_mk_settings' );
            do_settings_sections( 'recipes-options' );
            submit_button();
        ?>
    </form>
<?php }

add_action('init', 'init_recipes_maker');

function init_recipes_maker()
{
    $RCP_MK_rating = new RCP_MK_rating();

    $RCP_MK_manage = new RCP_MK_manage();
    $RCP_MK_manage->RCP_MK_insert_button_popup();

    $RCP_MK_assets = new RCP_MK_assets();
    $RCP_MK_assets->RCP_MK_load_resources();
}

?>