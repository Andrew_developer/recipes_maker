<?php

if (!function_exists('plugin_dir_url')) {
    require_once ABSPATH . WPINC . '/plugin.php';
}

class RCP_MK_manage
{
    function __construct()
    {
        add_action('admin_footer', array(__CLASS__, 'RCP_MK_insert_popup'));

        add_action('edit_form_advanced', 'myprefix_edit_form_advanced');

        function myprefix_edit_form_advanced()
        {
            if (get_post_type() == 'rcp_mk_recipes') {
                echo '<div class="code-recipe-full">[recipes_maker_full id="' . get_the_ID() . '"]</div>';
            }
        }
    }

    public function RCP_MK_insert_button_popup()
    {
        function RCP_MK_button($args = array())
        {
            $target = 'content';

            $args = wp_parse_args($args, array(
                'target' => $target,
                'text' => __('Recipes Maker', 'Recipes Maker'),
                'echo' => true,
                'shortcode' => false
            ));

            $button = '<a class="popup-button-recipces button" title="' . $args['text'] . '" data-target="' . $args['target'] . '" data-mfp-src="#popup-recipes-shortcode">' . $args['text'] . '</a>';

            if ($args['echo']) echo $button;

            return $button;
        }


        add_action('media_buttons', 'RCP_MK_button', 1000);
    }

    public static function RCP_MK_insert_popup()
    {
        function load_terms($taxonomy)
        {
            global $wpdb;
            $query = 'SELECT DISTINCT 
                  t.name,
                  t.term_id
              FROM
                ' . $wpdb->prefix . 'terms t 
              INNER JOIN 
                ' . $wpdb->prefix . 'term_taxonomy tax 
              ON 
                tax.term_id = t.term_id
              WHERE 
                  ( tax.taxonomy = \'' . $taxonomy . '\')';
            $result = $wpdb->get_results($query, ARRAY_A);
            return $result;
        }

        $categories = load_terms('rcp_mk_recipes_cat'); ?>

        <div id="popup-recipes-shortcode" class="popup-recipes-shortcode">

            <div class="popup-recipes-inner">
                <select id="select-cat-recipes" name="categories-recipes" multiple>
                    <?php
                    if ($categories) {
                        foreach ($categories as $cat) { ?>
                            <option value="<?php echo $cat['term_id']; ?>"> <?php echo $cat['name']; ?> </option>
                        <?php }
                    }
                    ?>
                </select>
            </div>

            <button class="insert-shortdode-btn">
                Insert ShortCode
            </button>

        </div>

    <?php }
} ?>