<?php

class RCP_MK_assets
{
    private $path;

    private $url;

    public function __construct( )
    {
        $this->url = plugins_url('', __FILE__);
    }

    public static function getPatchTemplate(){
        return plugins_url('../templates', __FILE__);
    }


    public function RCP_MK_load_resources()
    {
        add_action('admin_enqueue_scripts', 'RCP_MK_register_res');

        function RCP_MK_register_res()
        {
            wp_register_style('RCP_MK__PluginStylesheet', plugins_url('../css/magnific-popup.css', __FILE__));
            wp_enqueue_style('RCP_MK__PluginStylesheet');

            wp_register_style('RCP_MK__jqueryUiStyleplugin', plugins_url('../css/jquery-ui.css', __FILE__));
            wp_enqueue_style('RCP_MK__jqueryUiStyleplugin');

            wp_register_style('RCP_MK__css_admin', plugins_url('../css/css_admin.css', __FILE__));
            wp_enqueue_style('RCP_MK__css_admin');

            wp_register_script('RCP_MK__js_jquery-uiJS', plugins_url('../js/jquery-ui.js', __FILE__));
            wp_enqueue_script('RCP_MK__js_jquery-uiJS');

            wp_register_script('RCP_MK__js', plugins_url('../js/javascript-admin.js', __FILE__));
            wp_enqueue_script('RCP_MK__js');

            wp_register_script('RCP_MK__js_common', plugins_url('../js/javascript-common.js', __FILE__));
            wp_enqueue_script('RCP_MK__js_common');

            wp_register_script('RCP_MK__js_popup', plugins_url('../js/jquery.magnific-popup.js', __FILE__));
            wp_enqueue_script('RCP_MK__js_popup');
        }

        add_action( 'wp_enqueue_scripts', 'RCP_MK_load_scripts' );

        function RCP_MK_load_scripts() {
            wp_enqueue_style( 'style-bootstrap', plugins_url('../css/bootstrap.css', __FILE__) );

            wp_register_script('RCP_MK__js_jquery-uiJS', plugins_url('../js/jquery-ui.js', __FILE__));
            wp_enqueue_script('RCP_MK__js_jquery-uiJS');

            wp_register_script('RCP_MK__js_common', plugins_url('../js/javascript-common.js', __FILE__));
            wp_enqueue_script('RCP_MK__js_common');

            $data = array(
                'upload_url' => admin_url('async-upload.php'),
                'ajax_url'   => admin_url('admin-ajax.php'),
                'nonce'      => wp_create_nonce('media-form')
            );

            wp_localize_script( 'RCP_MK__js_common', 'su_config', $data );

            wp_register_script('RCP_MK__js_frontend', plugins_url('../js/js_frontend.js', __FILE__));
            wp_enqueue_script('RCP_MK__js_frontend');

            wp_enqueue_style( 'style-rating', plugins_url('../css/rating.css', __FILE__) );

            wp_enqueue_style( 'style-recipe-frontend', plugins_url('../css/recipe-frontend.css', __FILE__) );

            wp_enqueue_script( 'script-rating', plugins_url('../js/rating.js', __FILE__), array(), '1.0.0', true );

            wp_localize_script( 'script-rating', 'my_ajax_object',
                array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
        }
    }
}
?>