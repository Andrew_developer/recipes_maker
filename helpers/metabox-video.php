<?php

new Created_Metaboxes_Video_fields;

class Created_Metaboxes_Video_fields extends RCP_MK_metabox
{

    public $post_type = 'rcp_mk_recipes';

    static $meta_key = 'video';

    public function __construct()
    {
        add_action('add_meta_boxes', array($this, 'add_metabox'));
        add_action('save_post_' . $this->post_type, array($this, 'save_metabox'));
    }

    public function add_metabox()
    {
        add_meta_box('video', 'Custom', array($this, 'render_metabox'), $this->post_type, 'advanced', 'high');
    }


    public function render_metabox($post)
    {

        ?>
        <div class="additional-meta-box">

            <div class="item-additional">
                <label>Add Video</label>
                <input type="text" name="video[]"
                       value="<?php echo get_post_meta($post->ID, self::$meta_key, 1)[0]; ?>">
            </div>
        </div>

        <?php
    }

    public function save_metabox($post_id)
    {
        if (wp_is_post_autosave($post_id))
            return;

        if (isset($_POST[self::$meta_key]) && is_array($_POST[self::$meta_key])) {
            $process = $_POST[self::$meta_key];

            $process = array_map('sanitize_text_field', $process); // очистка

            $process = array_filter($process); // уберем пустые адреса

            if ($process) {
                update_post_meta($post_id, self::$meta_key, $process);
            } else {
                delete_post_meta($post_id, self::$meta_key);
            }
        }
    }


}