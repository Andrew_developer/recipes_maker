<?php

new Created_Metaboxes_custom_fields;

class Created_Metaboxes_custom_fields extends RCP_MK_metabox
{
    public $post_type = 'rcp_mk_recipes';

    static $meta_key = 'additional';

    public function __construct()
    {
        add_action('add_meta_boxes', array($this, 'add_metabox'));
        add_action('save_post_' . $this->post_type, array($this, 'save_metabox'));
    }

    public function add_metabox()
    {
        add_meta_box('additional', 'Custom', array($this, 'render_metabox'), $this->post_type, 'advanced', 'high');
    }

    public function render_metabox($post)
    { ?>
        <div class="additional-meta-box">

            <div class="item-additional">
                <label>Servings</label>
                <input type="text" name="additional[]"
                       value="<?php echo get_post_meta($post->ID, self::$meta_key, 1)[0]; ?>">
            </div>

            <div class="item-additional">
                <label>Calories</label>
                <input type="text" name="additional[]"
                       value="<?php echo get_post_meta($post->ID, self::$meta_key, 1)[1]; ?>">
            </div>

            <div class="item-additional">
                <label>Prep Time</label>
                <input type="text" name="additional[]"
                       value="<?php echo get_post_meta($post->ID, self::$meta_key, 1)[2]; ?>">
            </div>

            <div class="item-additional">
                <label>Cook Time</label>
                <input type="text" name="additional[]"
                       value="<?php echo get_post_meta($post->ID, self::$meta_key, 1)[3]; ?>">
            </div>

            <div class="item-additional">
                <label>Total Time</label>
                <input type="text" name="additional[]"
                       value="<?php echo get_post_meta($post->ID, self::$meta_key, 1)[4]; ?>">
            </div>

            <div class="item-additional">
                <label>Rating:</label>
                <?php
                    $total = get_post_meta($post->ID, 'vote-total', 1);
                    $rating = get_post_meta($post->ID, 'vote-rating', 1);

                    if($total == 0){
                        $pr = 0;
                        $abs = 0;
                    }
                    else{
                        $pr = ($rating / ($total * 5)) * 100;
                        $abs = round($rating / $total, 1);
                    }


                ?>
                <input type="text" readonly name="additional[]"
                       value="<?php echo get_post_meta($post->ID, 'vote-total', 1); ?> of votes, average: <?php echo $abs; ?> ">
            </div>
        </div>

        <?php
    }

    public function save_metabox($post_id)
    {
        if (wp_is_post_autosave($post_id))
            return;

        if (isset($_POST[self::$meta_key]) && is_array($_POST[self::$meta_key])) {
            $process = $_POST[self::$meta_key];

            $process = array_map('sanitize_text_field', $process);

            $process = array_filter($process);

            if ($process) {
                update_post_meta($post_id, self::$meta_key, $process);
            } else {
                delete_post_meta($post_id, self::$meta_key);
            }
        }
    }
}