<?php

new Created_Metaboxes_process;

class Created_Metaboxes_process extends RCP_MK_metabox
{

    public $post_type = 'rcp_mk_recipes';

    static $meta_key = 'process';

    public function __construct()
    {
        add_action('add_meta_boxes', array($this, 'rcp_mk_add_metabox'));
        add_action('save_post_' . $this->post_type, array($this, 'rcp_mk_save_metabox'));
//        add_action('admin_print_footer_scripts', array($this, 'rcp_mk_show_assets'), 10, 999);
    }

    public function rcp_mk_add_metabox()
    {
        add_meta_box('process', 'Process', array($this, 'rcp_mk_render_metabox'), $this->post_type, 'advanced', 'high');
    }

    public function rcp_mk_render_metabox($post)
    {

        ?>
        <table class="form-table metabox-recipes-info">

            <tr>
                <th>
                    Process Add <span class="dashicons dashicons-plus-alt add-custom-metabox"></span>

                    <button type="button" class="add-headline-btn ui-button">Add Headline</button>
                </th>
            </tr>

            <tr>
                <td class="recipes-item-list" id="sortable-i">
                    <?php
                    $input = '
					<span class="item-metabox ui-state-default">
						<input data-iter="%s" type="text" name="' . self::$meta_key . '[%s][des]" value="%s">
						
						<input class="image-input" type="hidden" name="' . self::$meta_key . '[%s][image]" value="%s">

						<span class="dashicons dashicons-trash remove-metabox-rec"></span>
						
						<img class="image-preview" width="50" src="%s">
						
						<button type="button" class="add-image-rec ui-button">Add img</button>	
					</span>
					';

                    $inputHeadline = '
					<span class="item-metabox headline-item ui-state-default">
					    <span class="title-headline">headline</span>
						<input data-iter="%s" type="text" name="' . self::$meta_key . '[%s][des]" value="%s">
						
						<input class="image-input" type="hidden" name="' . self::$meta_key . '[%s][image]" value="%s">

						<span class="dashicons dashicons-trash remove-metabox-rec"></span>	
					</span>
					';

                    $ingredients = get_post_meta($post->ID, self::$meta_key, true);

                    if (is_array($ingredients)) {
                        $i = 0;
                        foreach ($ingredients as $addr) {
                            if (esc_attr($addr['image']) == 'headline') {
                                printf($inputHeadline, $i, $i, esc_attr($addr['des']), $i, esc_attr($addr['image']));
                            } else {
                                printf($input, $i, $i, esc_attr($addr['des']), $i, esc_attr($addr['image']), esc_attr($addr['image']));
                            }

                            $i++;
                        }
                    } else {
                        printf($input, 0, 0, '', 0, '', '');
                    }
                    ?>
                </td>

            </tr>
        </table>

        <?php
    }

    public function rcp_mk_save_metabox($post_id)
    {
        if (wp_is_post_autosave($post_id))
            return;

        if (isset($_POST[self::$meta_key]) && is_array($_POST[self::$meta_key])) {
            $process = $_POST[self::$meta_key];

//            $process = array_map('sanitize_text_field', $process); // очистка
//
//            $process = array_filter($process); // уберем пустые адреса

            if ($process) {
                update_post_meta($post_id, self::$meta_key, $process);
            } else {
                delete_post_meta($post_id, self::$meta_key);
            }
        }
    }
}