<?php

new Created_Metaboxes_nutrition;

class Created_Metaboxes_nutrition extends RCP_MK_metabox
{

    public $post_type = 'rcp_mk_recipes';

    static $meta_key = 'nutrition';

    public function __construct()
    {
        add_action('add_meta_boxes', array($this, 'add_metabox'));
        add_action('save_post_' . $this->post_type, array($this, 'save_metabox'));
        add_action('admin_print_footer_scripts', array($this, 'show_assets'), 10, 999);
    }

    public function add_metabox()
    {
        add_meta_box('nutrition', 'Nutrition Information', array($this, 'render_metabox'), $this->post_type, 'advanced', 'high');
    }

    public function show_styles()
    {
        ?>
        <style>
            .add-process {
                color: #00a0d2;
                cursor: pointer;
            }

            .process-list .item-process {
                display: flex;
                align-items: center;
            }

            .process-list .item-process input {
                width: 100%;
                max-width: 400px;
            }

            .remove-cingredients {
                color: brown;
                cursor: pointer;
            }

            h5 {
                margin: 5px 0;
            }
        </style>
        <?php
    }

    public function render_metabox($post)
    {

        ?>

        <div class="wrap-list-nutrition">
            <div>
                <h4>Add Nutrition <span class="add-nutrition dashicons dashicons-plus-alt"></span></h4>
            </div>
            <table class="nutrition-info">

                <?php
                $input = '
                    <tr>
                        <td>
                            <h5>Name</h5>
                            <span class="item-sub-nutrition">
                                <input type="text" name="' . self::$meta_key . '[]" value="%s">
                            </span>
                        </td>
                        
                        <td>
                            <h5>Value</h5>
                            <span class="item-sub-nutrition">
                                <input type="text" name="' . self::$meta_key . '[]" value="%s">
                            </span>
                        </td>
                                                
                        <td style="padding-top: 34px;" class="remove-nutrition dashicons dashicons-trash"></td>
                    </tr>
					';

                $nutrition = get_post_meta($post->ID, self::$meta_key, true);

                if (is_array($nutrition)) {
                    $nutrition = array_chunk($nutrition, 2);
                }

                if (is_array($nutrition)) {
                    foreach ($nutrition as $addr) {
                        printf($input, esc_attr($addr[0]), esc_attr($addr[1]));
                    }
                } else {
                    printf($input, '', '');
                }
                ?>


            </table>
        </div>

        <?php
    }

    public function save_metabox($post_id)
    {
        if (wp_is_post_autosave($post_id))
            return;

        if (isset($_POST[self::$meta_key]) && is_array($_POST[self::$meta_key])) {
            $nutrition = $_POST[self::$meta_key];

            $nutrition = array_map('sanitize_text_field', $nutrition);

            $nutrition = array_filter($nutrition);

            if ($nutrition) {
                update_post_meta($post_id, self::$meta_key, $nutrition);
            } else {
                delete_post_meta($post_id, self::$meta_key);
            }
        }
    }

    public function show_assets()
    {
        if (is_admin() && get_current_screen()->id == $this->post_type) {
            $this->show_styles();
            $this->show_scripts();
        }
    }


    public function show_scripts()
    {
        ?>
        <script>
            jQuery(document).ready(function ($) {

                var $nutritionInfo = $('.nutrition-info');

                $('.add-nutrition').click(function () {
                    var $list = $('.nutrition-info');
                    $item = $list.find('tr').first().clone();

                    $item.find('input').val('');

                    $list.append($item);
                });

                $nutritionInfo.on('click', '.remove-nutrition', function () {
                    if ($('.wrap-list-nutrition tr').length > 1) {
                        $(this).closest('tr').remove();
                    }
                    else {
                        $(this).closest('tr').find('input').val('');
                    }
                });

            });
        </script>
        <?php
    }

}