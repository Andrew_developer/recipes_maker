<?php

function RCP_MK_func_shortcode($atts)
{
    ob_start();

    if ($atts['cat'] != 'all') {
        $catArray = explode(',', $atts['cat']);
        $options = array(
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'rcp_mk_recipes_cat',
                    'field' => 'id',
                    'terms' => $catArray,
                    'operator' => 'IN',
                )
            )
        );

    } else {
        $options = array(
            'post_type' => 'rcp_mk_recipes',
            'taxonomy' => 'rcp_mk_recipes_cat',
            'posts_per_page' => 1000,
            'paged' => 1,
        );
    }

    $query = new WP_Query($options);

    while ($query->have_posts()) {
        $query->the_post(); ?>

        <div class="container-recipes">

            <div style="overflow: hidden;" class="recipes-list">

                <h3 style="width: 70%; float: right;" id="post-<?php the_ID(); ?>">
                    <a href="<?php the_permalink(); ?>"> <?php echo get_the_title(); ?> </a>
                </h3>

                <a style="width: 30%; float: left;" href="<?php the_permalink(); ?>">
                    <?php the_post_thumbnail('thumbnail'); ?>
                </a>

            </div>

        </div>

    <?php }

    wp_reset_postdata();

    $template = ob_get_clean();
    return $template;
}

function RCP_MK_full_recipe($atts)
{
    global $post;
    ob_start();

    $id = intval($atts['id']);

    $args = array(
        'p' => $id,
        'post_type' => 'rcp_mk_recipes'
    );
    $query = new WP_Query($args);

    $templateNumber = get_option('rcp_mk_options_template')['radio'];

    $filepath = 'templates/content-recipe.php';

    switch ($templateNumber) {
        case 1:
            $filepath = 'templates/content-recipe.php';
            break;
        case 2:
            $filepath = 'templates/content-recipe-second.php';
            break;
        case 3:
            $filepath = 'templates/content-recipe-third.php';
            break;
        case 4:
            $filepath = 'templates/content-recipe-four.php';
            break;
    }

    if ($query->have_posts()) {
        $query->the_post();

        include $filepath;
    };


    $template = ob_get_clean();
    return $template;
}

function RCP_MK_form_add_shortcode($attr)
{

    ob_start();

    ?>


<div class="form-added-recipe image-form">
    <form name="" method="post" action="">
        <label>Title</label>
        <input type="text" name="title">

        <label>Description</label>
        <textarea name="content"></textarea>

        <?php include 'templates/content-ingridients.php'; ?>

        <?php include 'templates/content-process.php'; ?>

        <input type="submit" value="Submit">
    </form>
</div>

    <?php $template = ob_get_clean();

    $post_id = wp_insert_post(wp_slash(array(
        'post_status' => 'draft',
        'post_type' => 'rcp_mk_recipes',
        'post_content' => $_POST['content'],
        'post_title' => wp_strip_all_tags($_POST['title']),
        'ping_status' => get_option('default_ping_status'),
        'post_parent' => 0,
        'menu_order' => 0,
        'to_ping' => '',
        'pinged' => '',
        'post_password' => '',
        'post_excerpt' => '',
    )));

    update_post_meta( $post_id, 'ingredients', $_POST['ingredients'] );

    update_post_meta( $post_id, 'process', $_POST['process'] );

    return $template;
}

add_shortcode('recipes_maker_shortcode', 'RCP_MK_func_shortcode');

add_shortcode('recipes_maker_full', 'RCP_MK_full_recipe');

add_shortcode('recipes_maker_add_shortcode', 'RCP_MK_form_add_shortcode');

?>